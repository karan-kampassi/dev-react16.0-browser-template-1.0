FROM node:12.18.4-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./

RUN npm install --silent --only=production

# add app
COPY . ./

# start app
CMD ["npm", "start"]